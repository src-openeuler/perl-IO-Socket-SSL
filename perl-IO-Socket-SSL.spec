Name:		perl-IO-Socket-SSL
Version:	2.089
Release:	2
Summary:	Perl library for transparent SSL
License:	GPL-1.0-or-later OR Artistic-1.0-Perl
URL:		https://metacpan.org/release/IO-Socket-SSL
Source0:	https://cpan.metacpan.org/authors/id/S/SU/SULLR/IO-Socket-SSL-%{version}.tar.gz

BuildArch:	noarch
#For Build
BuildRequires:	coreutils findutils make perl-generators perl-interpreter perl(ExtUtils::MakeMaker)
# For Runtime
BuildRequires:	openssl >= 0.9.8 perl(Carp) perl(Config) perl(constant) perl(Errno) perl(Exporter)
BuildRequires:	perl(HTTP::Tiny) perl(IO::Socket) perl(Net::SSLeay) >= 1.46
BuildRequires:	perl(Scalar::Util) perl(Socket) perl(strict) perl(vars) perl(warnings)
# For Test
BuildRequires:	perl(Data::Dumper) perl(File::Temp) perl(FindBin) perl(IO::Select)
BuildRequires:	perl(IO::Socket::INET) perl(Test::More) >= 0.88 perl(utf8) procps
BuildRequires:	perl(IO::Socket::IP) >= 0.20, perl(Socket) >= 1.95 perl(URI::_idna)

Requires:	openssl >= 0.9.8
Requires:	perl(Config)
Requires:	perl(HTTP::Tiny)
Requires:	perl(IO::Socket::IP) >= 0.20, perl(Socket) >= 1.95
Requires:	perl(URI::_idna)

%description
IO::Socket::SSL is a class implementing an object oriented
interface to SSL sockets. The class is a descendent of
IO::Socket::INET.

%package_help

%prep
%autosetup -n IO-Socket-SSL-%{version} -p1

%build
perl Makefile.PL INSTALLDIRS=vendor NO_NETWORK_TESTING=1 NO_PACKLIST=1
%make_build

%install
make pure_install DESTDIR=%{buildroot}
%{_fixperms} -c %{buildroot}

%check
make test

%files
%doc BUGS Changes README docs/ example/
%dir %{perl_vendorlib}/IO/
%dir %{perl_vendorlib}/IO/Socket/
%doc %{perl_vendorlib}/IO/Socket/SSL.pod
%{perl_vendorlib}/IO/Socket/SSL.pm
%{perl_vendorlib}/IO/Socket/SSL/

%files help
%{_mandir}/man3/IO::Socket::SSL.3*
%{_mandir}/man3/IO::Socket::SSL::Intercept.3*
%{_mandir}/man3/IO::Socket::SSL::PublicSuffix.3*
%{_mandir}/man3/IO::Socket::SSL::Utils.3*

%changelog
* Sat Jan 18 2025 Funda Wang <fundawang@yeah.net> - 2.089-2
- drop useless perl(:MODULE_COMPAT) requirement

* Wed Jan 08 2025  openeuler_bot <infra@openeuler.sh> - 2.089-1
- Changes for version 2.089 - 2024-08-29
  - new option SSL_force_fingerprint to enforce fingerprint matching even if certificate validation would be successful without
  - document _get_ssl_object and _get_ctx_object for cases, where direct use of Net::SSLeay functions is needed

* Wed Jul 17 2024 dfh <dufuhang@kylinos.cn> - 2.088-1
- update version to 2.088
- minor fixes for use on ancient versions of perl and for building with newer versions of openssl

* Mon Jan 29 2024 dongyuzhen <dongyuzhen@h-partners.com> - 2.084-1
- upgrade version to 2.084:
  - various fixes for edge cases
  - update documentation to reflect default SSL_version

* Wed Jul 19 2023 dongyuzhen <dongyuzhen@h-partners.com> - 2.083-1
- upgrade version to 2.083

* Mon Feb 13 2023 dongyuzhen <dongyuzhen@h-partners.com> - 2.081-1
- upgrade version to 2.081

* Mon Oct 31 2022 dongyuzhen <dongyuzhen@h-partners.com> - 2.072-2
- Rebuild for next release

* Fri Nov 19 2021 yuanxin <yuanxin24@huawei.com> - 2.072-1
- update version to 2.072

* Fri Jul 24 2020 xinghe <xinghe1@huawei.com> - 2.068-1
- Type:enhancement
- ID:NA
- SUG:NA
- DESC:update version to 2.068

* Tue Dec 31 2019 openEuler Buildteam <buildteam@openeuler.org> - 2.066-4
- Type:enhancement
- ID:NA
- SUG:NA
- DESC:delete unneeded patch

* Tue Oct 15 2019 shenyangyang <shenyangyang4@huawei.com> - 2.066-3
- Type:enhancement
- ID:NA
- SUG:NA
- DESC:delete unneeded build requires

* Sat Oct 14 2019 shenyangyang <shenyangyang4@huawei.com> - 2.066-2
- Type: enhancement
- ID: NA
- SUG: NA
- DESC:delete the /certs that used by test which contains the Unencrypted
       private key

* Wed Sep 11 2019 openEuler Buildteam <buildteam@openeuler.org> - 2.066-1
- Package init
